import torch

def set_inflow_bc(div, U, flags, batch_dict):

    cuda = torch.device('cuda')
    assert (U.dim() == 5 and flags.dim() == 5), 'Dimension mismatch'
    assert flags.size(1) == 1, 'flags is not a scalar'
    bsz = flags.size(0)
    d = flags.size(2)
    h = flags.size(3)
    w = flags.size(4)

    is3D = (U.size(1) == 3)
    if (not is3D):
        assert d == 1, '2D velocity field but zdepth > 1'
        assert U.size(1) == 2, '2D velocity field must have only 2 channels'

    assert (U.size(0) == bsz and U.size(2) == d and U.size(3) == h and U.size(4) == w),\
        'Size mismatch'
    assert (U.is_contiguous() and flags.is_contiguous()), 'Input is not contiguous'

    corrected_div = (U[:,1,:,1:,:]*(1-batch_dict['densityBCInvMask'])[:,0,:,:-1,:] - batch_dict['UBC'][:,0,:,:-1,:]).unsqueeze(1)
    div[:,:,:,1:] -= corrected_div 

    print(div)

