import sys
import numpy as np
import matplotlib.pyplot as plt
import glob
import os

# Load numpy arrays
#assert (len(sys.argv) == 2), 'Usage: python3 plot_loss.py <plotDirName>'
#assert (len(sys.argv) == 3), 'Usage: python3 plot_loss.py <plotDirName>'
#assert (glob.os.path.exists(sys.argv[1])), 'Directory ' + str(sys.argv[1]) + ' does not exists'

def plot_loss(loss_type, network_list, res, min_max, folder, colors):

    line_s = ['solid', 'dotted']
    for i, network in enumerate(network_list):
        save_dir = glob.os.path.join(folder, network)
        file_name = glob.os.path.join(save_dir, '{}_loss.npy'.format(loss_type))
        train_loss_plot = np.load(file_name)

        color = colors[i%3]
        line = line_s[i//3]
        x_start = 0
        # Plot loss against epochs
        plt.plot(train_loss_plot[x_start:,0], train_loss_plot[x_start:,1], label = network, linestyle =line, color = color)

    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.title(loss_type)
    plt.ylim(min_max)
    plt.legend()
    save_name = glob.os.path.join(folder, 'losses_6_{}_{}.png'.format(loss_type, res))
    plt.savefig(save_name)
    plt.close()


data_res = [128]
colors = ['blue', 'red', 'green']

for res in data_res:

    network_list = ['FlexiNet_6_slim_{}'.format(res), 'FlexiNet_6_wide_{}'.format(res)]  #'FlexiNet_3_slim_{}'.format(res), 'FlexiNet_4_slim_{}'.format(res), 'FlexiNet_5_slim_{}'.format(res), 
                    #'FlexiNet_3_wide_{}'.format(res), 'FlexiNet_4_wide_{}'.format(res), 'FlexiNet_5_wide_{}'.format(res)]


    folder = '/tmpdir/ajuriail/fluidnet_pando/fluidnet_cxx_2/trained_models/calm_models'

    losses = ['train', 'val']
    min_max = (1.e-7, 1e-3)    

    for loss in losses:
        plot_loss(loss, network_list, res, min_max,folder,colors)
