# FluidNet_SUPAERO  
This repository is based on the paper, [Accelerating Eulerian Fluid Simulation With Convolutional Networks](http://cims.nyu.edu/~schlacht/CNNFluids.htm) by Jonathan Tompson, Kristofer Schlachter, Pablo Sprechmann, Ken Perlin on the accelation of fluid simulations by embedding a neural network in an existing solver for pressure prediction, replacing an expensive pressure projection linked to a Poisson equation on the pressure, which is usually solved with iterative methods (PCG or Jacobi methods). We implemented our code with PyTorch, effectively replacing all the original Torch/Lua and C++/CUDA implementation of the inviscid, incompressible fluid solver (based on the open-source fluid simulator [Mantaflow](http://mantaflow.com/), aimed at the Computer Graphics community).
Find the original FluidNet repository [here](https://github.com/google/FluidNet). Moroever, this code is the continuation of the work of Antonio Alguacil, who started this project during his internship at the Jolibrain company. The original github repository can be found [here](https://github.com/jolibrain/fluidnet_cxx) 

Different CNN architectures are available in the repository, as well as an already trained MultiScale network.

## Installation
To install this repo:

1. Clone this repo:
```
git clone https://gitlab.isae-supaero.fr/e.ajuria/fluidnet_supaero.git
```
2. Create a Python enironment with the correct requirements:

```bash
python -m venv path/to/your/env  
source path/to/your/env/bin/activate
pip install -r requirements.txt
```

3. Install cpp extensions for fluid solver:
C++ scripts have been written using PyTorch's backend C++ library ATen.
These scripts are used for the advection part of the solver.
Follow these instructions from main directory:

Load gcc compiler and cuda:

```
module load gcc/7.3.0 # 8.2.0 works as well!
module load cuda/10.0
```

Once both modules are loaded, just go the cpp file and install the library. Please note , that some issues might arise as the code might not find the cuda path, thus the CFLAGS option is added:

```
cd pytorch/lib/fluid/cpp
CFLAGS="-I${CUDA_PATH}/include -I${CUDA_PATH}/samples/common/inc " python3 setup.py install
```



## Functionalities:
* Full eulerian (incompressible and inviscid) fluid simulator:
    * Momentum equation resolution using a splitting algorithm:
        * Advection of velocity + External forces
        * Enforcing of non-divergence of velocity constraint through Poisson equation resolution, resulting in a pressure gradient
          that corrects the velocity from the previous step. Step replaced by a fully convolutional Neural Network with
          divergence of velocity as input and pressure as output.
    * Unconditional Stable MacCormack discretization of velocity advection algorithm.
    * Jacobi and CG method implementation for comparison.
* Dataset:
    * Generation with FluidNet own Mantaflow sript.
    * Random insertion of objects and velocity emitters, as well as gravity forces.
    * Pre-processed into PyTorch objects
* Pre-trained models:
    * An already trained model is available in [trained_models](trained_models)
* Training:
    * Several options for loss function: 
        * MSE of pressure 
        * "Physical" loss: MSE of velocity divergence (unsupervised)
        * MSE of velocity divergence after several timesteps.
    * Short term divergence loss: 8 hours training
    * Short+Long term divergence loss: ~2 days
* Inference. Several test cases:
    * Buoyant plume.
    * Rayleigh Taylor instability.
    * Flow around a cylinder (2D Von Karman Vortex Street).
    * Flow around 2 cylinders.
    * Backwards facing step.
    * Flow around a NACA0012.    

* Results visualization:
    * Matplotlib
    * Paraview post-processing tool (VTK files)

# Models

* **MultiScale**: Deep MultiScale adapted from [Deep multi-scale video prediction beyond mean square error](https://arxiv.org/abs/1511.05440).
* **FlexiUnet**: Unet network, adapted from [U-Net: Convolutional Networks for Biomedical Image Segmentation](https://arxiv.org/abs/1505.04597).
The network can be directly modifed from the config file, as the network scales can be defined as:

```
scales:
    depth_0: [[2, 32, 20], [40, 32, 1]]
    depth_1: [[20, 56], [96, 20]]
    depth_2: [[56, 30], [50, 40]]
    depth_3: [[30, 20, 20], [80, 10, 20]]
    depth_4: [20, 60]

```
* **MultiScaleNetSmall**: Reduced multiScale architecture found on lib/multi_scale_net_small.py

* **MonoScale**: Fixed MonoScale architecture found on lib/mono_scale.py

* **FlexiNet**: Flexible architecture, mixture between Unet and Multiscale. Its size is defined as:

```
filters: [
    [36, 36, 36, 36, 36, 32],
    [36, 36, 36, 36, 36, 32],
    [36, 36, 36, 36, 36, 32]
         ]

```


ATen allows to write generic code that works on both devices.
More information in ATen [repo](https://github.com/zdevito/ATen). It can be called from PyTorch, using its [extension-cpp](https://pytorch.org/tutorials/advanced/cpp_frontend.html).


Please Note that before performing any inference or training test, the gcc compiler should be loaded! (7.3.0 and 8.2.0 versions have been tested)

```
module load gcc/7.3.0
```

## Training

**Dataset**
We use the same **2D dataset** as the original FluidNet [Section 1: Generating the data - Generating training data](https://github.com/google/FluidNet#1-generating-the-data) (generated with MantaFlow) for training our ConvNet.

**Running the training**
To train the model, go to pytorch folder: 
```
cd pytorch
```
The dataset file structure should be located in ```<dataDir>``` folder with the following structure: 
```
.
└── dataDir
    └── dataset
        ├── te
        └── tr

```
Precise the location of the dataset in ```pytorch/config_files/trainConfig.yaml``` writing the folder location at ```dataDir``` (__use absolute paths__).
Precise also ```dataset``` (name of the dataset), and output folder ```modelDir```where the trained model and loss logs will be stored and the model name ```modelFilename```.

Run the training :
```
python3 fluid_net_train.py --trainingConf config_files/trainConfig.yaml
```
For a given dataset, a **pre-processing** operation must be performed to save it as PyTorch objects, easily loaded when training. This is done automatically if no preprocessing log is detected.
This process can take some time but it is necessary only once per dataset.

Training can be stopped using Ctrl+C and then resumed by running:
```
python3 fluid_net_train.py --trainingConf config_files/trainConfig.yaml --resume
```


### Training options
You can set the following options for training from the terminal command line:
* ```-h``` : displays help message
* ```--trainingConf``` : YAML config file for training. Default = config.yaml.
* ```--modelDir``` : Output folder location for trained model. When resuming, reads from this location.
* ```--modelFilename``` : Model name.
* ```--dataDir``` : Dataset location.
* ```--resume``` : Resumes training from checkpoint in ```modelDir```
* ```--bsz``` : Batch size for training.
* ```--maxEpochs``` : Maximum number training epochs.
* ```--noShuffle``` : Remove dataset shuffle when training.
* ```--lr``` : Learning rate.
* ```--numWorkers``` : Number of parallel workers for dataset loading.
* ```--outMode``` : Training debug options. Prints or shows validation dataset.
        ```save```  = saves plots to disk
        ```show```  = shows plots in window during training
        ```none```  = do nothing

The rest of the training parameters are set in the config_files/trainingConf file, by default [config_files/trainConfig.yaml](pytorch/config_files/trainConfig.yaml).

Parameters in the YAML config file are copied into a python dictionary and saved as two separated dictionaries in ```modelDir```, one conf dictionary for parameters related to training (batch size, maximum number of epochs) and one mconf dictionary for parameters related to the model (inputs, losses, scaling options etc) 

## Test
Run the buoyant plume test case by running:
```
cd pytorch
python3 plume.py --simConf config_files/Config_Plume.yaml
```
with:
* ``` <modelDir>``` : folder with trained model.
* ``` <modelFilename>``` : Trained model name.
* ``` <outputFolder>``` : Folder for saving simulation results.

You can also stop the simulation (Ctrl+C) and restart it afterwards:
```
python3 plume.py --simConf config_files/Config_Plume.yaml --restartSim
```

### Test options
* ```-h``` : displays help message
* ```--simConf``` : YAML config file for simulation. Default = plumeConfig.yaml.
* ```--trainingConf``` : YAML config file for training. Default = config.yaml.
* ```--modelDir``` : Trained model location.
* ```--modelFilename``` : Model name.
* ```--outputFolder``` : Location of output results.
* ```--restartSim``` : Restart simulation from checkpoint in ```<outputFolder>```.

Check [config_files/Config_Plume.yaml](pytorch/config_files/Config_Plume.yaml) to see how the configuation file for the simulation is organized.

## Modifying the NN architecture

If you want to try your own architecture, you only have to follow these simple rules:
* Write your model in a separate script and save it inside ```pytorch/lib```.
* Open ```model.py``` and import your own script as a module. Go to ```class FluidNet```
  [here](https://github.com/AAlguacil/fluidnet_cxx/blob/d09c192641daeb42668bdf2b70cfc1f415944e98/pytorch/lib/model.py#L42).
* Ideally, as with the Multi-Scale Net example, you should just have to precise the number of channels from the input,
  and add your net forward pass as in the multicale example
  [here](https://github.com/AAlguacil/fluidnet_cxx/blob/d09c192641daeb42668bdf2b70cfc1f415944e98/pytorch/lib/model.py#L175)

## Extending the cpp code:

The cpp code, written with ATen library, can be compiled, tested and run on its own.
You will need [OpenCV2](https://opencv.org/opencv-2-4-8.html) to visualize output of the pressure and velocity fields, as matplotlib is unfortunately not available in cpp!

**Test**

First, generate the test data from FluidNet
[Section 3. Limitations of the current system - Unit Testing](https://github.com/google/FluidNet#3-limitations-of-the-current-system) and write the location of your folder in:
```
solver_cpp/test/test_fluid.cpp
#define DATA <path_to_data>
```
Run the following commands:
```
cd solver_cpp/
mkdir build_test
cd build_test
cmake .. -DFLUID_TEST=ON # Default is OFF
./test/fluidnet_sim
```
This will test every routine of the solver (advection, divergence calculation, velocity
update, adding of gravity and buoyancy, linear system resolution with Jacobi method).
These tests are taken from FluidNet and compare outputs of Manta to ours, except for 
advection when there is no Manta equivalent. In that case, we compare to the original
FluidNet advection.

**Run**

```
cd solver_cpp/
mkdir build
cd build
cmake .. -DFLUID_TEST=OFF # Default is OFF
./simulate/fluidnet_sim
```
Output images will be written in ```build``` folder, and can be converted into gif using
ImageMagick.

**NOTE: For the moment, only 2D simulations and training are supported, as bugs are still
found for the 3D advection.**



